﻿using libNexmosphereManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace Nexmosphere_Manager_V2
{
    class Program
    {
        static string _listenPort;
        static LogManager logManager;
        static HttpListener _httpListener;
        static string output;
        static Thread _httpListenerThread;
        static NexmosphereManager nexmoManager;
        static string lastLog;

        //Fling=http://localhost:8081 COM=5 Modules=xdot_001,002,003;xeye200_007,008 listenPort=5742 Commands=G111B[0,99,96,86,0];G111B[1,6,48,6,0];G111B[2,0,44,80,0];G111B[3,31,8,71,0]
        static void Main(string[] args)
        {
            try
            {
                logManager = new LogManager();

                try
                {
                    Log("-");
                    Log($"Starting Nexmosphere Manager with args: {string.Join(", ", args)}");

                    Dictionary<string, string> dicArgs = Utils.argsToDictionary(args);
                    Dictionary<string, string> modules = null;
                    List<string> commands = null;
                    FlingManager flingManager = null;

                    if (!dicArgs.ContainsKey("COM"))
                    {
                        Log("Missing COM parameter");
                        return;
                    }
                    if (dicArgs.ContainsKey("Fling"))
                    {
                        flingManager = new FlingManager(dicArgs["Fling"]);
                    }
                    if(dicArgs.ContainsKey("Modules"))
                    {
                        modules = Utils.GetModules(dicArgs["Modules"]);
                    }
                    if (dicArgs.ContainsKey("listenPort"))
                    {
                        _listenPort = dicArgs["listenPort"];
                    }
                    else
                    {
                        _listenPort = "5742";
                    }
                    if (dicArgs.ContainsKey("Commands"))
                    {
                        commands = Utils.GetCommands(dicArgs["Commands"]);
                    }

                    nexmoManager = new NexmosphereManager(dicArgs["COM"]);
                    nexmoManager.OpenConnection();

                    NexmosphereStartupConfig(commands);

                    StartHttpThread();

                    Modes modes = new Modes(nexmoManager, flingManager, modules);
                    while (true)
                    {
                        try
                        {
                            if (nexmoManager.serialPort.IsOpen)
                            {
                                modes.Read(out output);
                            }
                            else
                            {
                                nexmoManager.OpenConnection();
                                Log($"Serial connection restored.");
                                NexmosphereStartupConfig(commands);
                                Log($"Startup Config refreshed.");
                            }
                        }
                        catch (Exception ex)
                        {
                            Log($"ERROR - Failed serial connection.\n{ex.Message} - {ex.StackTrace}");
                            Thread.Sleep(5000);
                            continue;
                        }                                
                    }                    
                }
                catch (Exception e)
                {
                    Log($"{e.Message} - {e.StackTrace}");
                }
                finally
                {
                    Log("-");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message} - {e.StackTrace}");
            }            
        }

        private static void NexmosphereStartupConfig(List<string> commands)
        {
            if (commands != null)
            {
                foreach (string command in commands)
                {
                    //logManager.Log($"Sending {command}...");
                    nexmoManager.Send(command);
                    Thread.Sleep(100);
                }
            }
        }

        private static void StartHttpThread()
        {
            _httpListener = new HttpListener();
            _httpListener.Prefixes.Add($"http://+:{_listenPort}/");
            _httpListener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;

            try
            {
                _httpListener.Start();
                Log("Server listening...");
                _httpListenerThread = new Thread(() => {
                    while (_httpListener.IsListening)
                    {
                        try
                        {
                            HttpCallHandler(_httpListener.GetContext(), nexmoManager);
                        }
                        catch (Exception ex)
                        {
                            Log(ex.ToString());
                        }
                    }
                });
                _httpListenerThread.Start();
            }
            catch (HttpListenerException ex)
            {
                Log($"Error starting http listener. Error: \r\n {ex.StackTrace}\r\n In caso di 'Access is denied', esegui qusto comando: 'netsh http add urlacl url=http://+:{_listenPort}/user=<username>'");
            }
        }

        private static void Log(string text)
        {
            if (lastLog == text) return;
            Console.WriteLine(text);
            logManager.Log(text);
            lastLog = text;
        }

        private static void HttpCallHandler(HttpListenerContext context, NexmosphereManager nexmosphereManager)
        {
            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;

            if (request.Headers["Origin"] != null)
            {
                response.AddHeader("Access-Control-Allow-Origin", request.Headers["Origin"]);
                response.AddHeader("Access-Control-Allow-Methods", "POST");
            }

            switch (request.HttpMethod)
            {
                case "PUT":
                    if (!request.HasEntityBody)
                    {
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        response.StatusDescription = "Missing body";
                        response.Close();
                        return;
                    }
                    // read body
                    using (var streamReader = new StreamReader(request.InputStream, request.ContentEncoding))
                    {
                        String body = streamReader.ReadToEnd();
                        if (String.IsNullOrWhiteSpace(body))
                        {
                            return;
                        }
                        //Log($"Sending {body}...");
                        if (nexmosphereManager.serialPort.IsOpen)
                        {
                            nexmosphereManager.Send(body);
                            response.StatusCode = (int)HttpStatusCode.OK;
                            response.StatusDescription = HttpStatusCode.OK.ToString();
                            SendString("OK", response);
                        }
                        else
                        {
                            response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            response.StatusDescription = HttpStatusCode.InternalServerError.ToString();
                            SendString("ERROR - serial connection not available", response);
                        }
                    }
                    break;
                case "GET":
                    response.StatusCode = (int)HttpStatusCode.OK;
                    response.StatusDescription = HttpStatusCode.OK.ToString();
                    SendString(output, response);
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                    response.StatusDescription = HttpStatusCode.MethodNotAllowed.ToString();
                    response.Close();
                    break;
            }
        }

        private static void SendString(String str, HttpListenerResponse response)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(str + "\n");
            response.ContentType = "application/json";
            response.ContentEncoding = Encoding.UTF8;
            response.ContentLength64 = buffer.Length;
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.OutputStream.Close();
        }
    }
}
