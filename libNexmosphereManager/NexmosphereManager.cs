﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace libNexmosphereManager
{
    public class NexmosphereManager : IDisposable
    {
        public string COMport { get; set; }
        public int BaudRate { get; set; }
        public Parity parity { get; set; }
        public int dataBits { get; set; }
        public StopBits stopBits { get; set; }
        public SerialPort serialPort { get; set; }

        public NexmosphereManager(string COM)
        {
            COMport = $"COM{COM}";
            BaudRate = 115200;
            parity = Parity.None;
            dataBits = 8;
            stopBits = StopBits.One;
            serialPort = new SerialPort(COMport, BaudRate, parity, dataBits, stopBits);
        }

        public void OpenConnection()
        {
            serialPort.Open();          
        }

        public void CloseConnection()
        {
            serialPort.Close();
        }

        public string Read()
        {
            return serialPort.ReadLine();
        }

        public void Send(string message)
        {
            serialPort.Write($"{message}\r\n");
        }

        public void Dispose()
        {
            if (serialPort.IsOpen) serialPort.Close();
            serialPort.Dispose();
        }
    }
}
