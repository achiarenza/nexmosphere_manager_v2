﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace libNexmosphereManager
{
    public class Modes
    {
        private NexmosphereManager nexmosphereReader { get; set; }
        private Dictionary<string, string> modules { get; set; }
        private FlingManager flingManager { get; set; }
        private Dictionary<string, SensorStatus> moduleStatus { get; set; }

        public Modes(NexmosphereManager nexmosphereReader, FlingManager flingManager = null, Dictionary<string, string> modules = null)
        {
            this.nexmosphereReader = nexmosphereReader;
            this.flingManager = flingManager;
            this.modules = modules;
            if (this.modules != null)
            {
                moduleStatus = new Dictionary<string, SensorStatus>();
                foreach (string moduleIndex in modules.Keys)
                {
                    moduleStatus.Add(moduleIndex, new SensorStatus(moduleIndex, modules[moduleIndex], ""));
                }
            }
        }

        public void GetStatus()
        {
            if (moduleStatus != null)
            {
                foreach (string moduleIndex in moduleStatus.Keys)
                {
                    if (moduleStatus[moduleIndex].Type == "xeye200")
                    {
                        
                    }
                }
            }
        }

        public void Read(out string output)
        {
            string line;
            string moduleIndex;
            string value;
            List<SensorStatus> sensorStatuses = new List<SensorStatus>();

            if (modules == null)
            {
                    line = nexmosphereReader.Read();
                    Console.WriteLine(line);
                    output = line;
                    if (!(flingManager == null))
                    {
                        flingManager.SendPost(line);
                    }
            }
            else
            {
                while (true)
                {                    
                    line = nexmosphereReader.Read();
                    moduleIndex = line.Substring(1, 3);
                    if (!modules.ContainsKey(moduleIndex))
                    {
                        continue;
                    }

                    value = Modules.GetStatus(line);

                    if (moduleStatus[moduleIndex].Type == "button" && value == "0") continue;

                    moduleStatus[moduleIndex].Value = value;

                    foreach(string module in moduleStatus.Keys)
                    {
                        sensorStatuses.Add(moduleStatus[module]);                       
                    }

                    output = JsonConvert.SerializeObject(sensorStatuses);

                    if (!(flingManager == null))
                    {
                        flingManager.SendPost(output);
                    }

                    sensorStatuses.Clear();
                }
            }
        }
    }
}
