﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNexmosphereManager
{
    static public class Utils
    {
        static public Dictionary<string, string> argsToDictionary(string[] args)
        {
            return args.Select(a => a.Split('=')).ToDictionary(a => a[0], a => a.Length == 2 ? a[1] : null);
        }

        static public Dictionary<string, string> GetModules(string modules)
        {
            Dictionary<string, string> res = new Dictionary<string, string>();
            foreach (string typeAndList in modules.Split(';'))
            {
                string type = typeAndList.Split('_')[0];
                string list = typeAndList.Split('_')[1];
                foreach (string item in list.Split(',')) res.Add(item, type);
            }
            //return modules.Split(';').Select(a => a.Split(',')).ToDictionary(a => a[0], a => a.Length == 2 ? a[1] : null);
            return res;
        }

        static public List<string> GetCommands(string commands)
        {
            return commands.Split(';').ToList();
        }
    }
}
