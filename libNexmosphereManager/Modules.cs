﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace libNexmosphereManager
{
    static public class Modules
    {
        static public string GetXdotStatus(string output)
        {
            string value = Regex.Match(output, @"\[(.+?)\]").Groups[1].Value;
            if (value == "3" || value == "7")
            {
                return "Pickup";
            }
            else
            {
                return "NoPickup";
            }
        }

        static public string GetXeye200Status(string output)
        {
            string value = Regex.Match(output, @"\[(.+?)\]").Groups[1].Value.Replace("Dz=", "");
            if (value == "XX")
            {
                return "Nobody";
            }
            if (value == "AB")
            {
                return "Button";
            }
            return value;
        }

        static public string GetXeyeStatus(string output)
        {
            string value = Regex.Match(output, @"\[(.+?)\]").Groups[1].Value;
            return value;
        }

        static public string GetStatus(string output)
        {
            return Regex.Match(output, @"\[(.+?)\]").Groups[1].Value;
        }
    }
}
