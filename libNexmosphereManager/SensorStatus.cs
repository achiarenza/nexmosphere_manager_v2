﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNexmosphereManager
{
    public class SensorStatus
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public SensorStatus(string Id, string Type, string Value)
        {
            this.Id = Id;
            this.Type = Type;
            this.Value = Value;
        }
    }
}
