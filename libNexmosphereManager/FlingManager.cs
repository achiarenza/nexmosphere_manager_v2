﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScairimCommons;

namespace libNexmosphereManager
{
    public class FlingManager
    {
        private string Address { get; set; }
        private object[,] headers { get; set; }

        public FlingManager(string Address)
        {
            this.Address = Address;         
        }

        public string SendPost(string body)
        {
            headers = new object[2, 2];
            headers[0, 0] = "Content-Type";
            headers[0, 1] = "application/text";
            headers[1, 0] = "Content-Length";
            headers[1, 1] = body.Length;
            return Http.Send(body, Address, headers, "POST", "text");
        }

        public string SendDelete()
        {
            return Http.Send("", Address, "DELETE", "text");
        }
    }
}
